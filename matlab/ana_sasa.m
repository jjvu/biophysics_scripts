%%
clear all

%Description: Calculating solvent accessible surface values for protein domains
%Notes: Assumes specific input data structure (GROMACS).

n=1;

simulSize=101;
names = dir('1-*');
ac = {names(:).name}';
match = ["1-sasa_",".xvg"];
newStr = erase(ac,match);
nAnal = max(size(newStr));
s=struct('names',newStr);
[s(:).data]=deal(zeros(simulSize,n));
[s(:).data2]=deal(zeros(simulSize,n));
[s(:).data3]=deal(zeros(simulSize,2));

for i = 1:1:nAnal
for l = 1:1:n
    
    fid = fopen(sprintf('%i-sasa_%s.xvg',l,s(i).names));
    for k=1:25
        fgetl(fid);
    end
    c = textscan(fid,'%f%f%f','Delimiter','\n');
    s(i).data(:,l) = c{1,2};
    fclose(fid);
    
end
end

for j = 1:1:nAnal
    for i=1:1:nAnal
        for l = 1:1:n
            for time=1:1:simulSize
                
                if s(j).names == "JAK2_1_FERM-SH2" && s(i).names == "notF1"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_2_FERM-SH2" && s(i).names == "notF2"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_1_PK" && s(i).names == "notP1"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_2_PK" && s(i).names == "notP2"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);     
                end
                
                if s(j).names == "JAK2_1_TK" && s(i).names == "notT1"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_2_TK" && s(i).names == "notT2"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_FERM-SH2" && s(i).names == "notF"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_PK" && s(i).names == "notP"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "JAK2_TK" && s(i).names == "notT"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_1_TM" && s(i).names == "notTM1"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_2_TM" && s(i).names == "notTM2"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_1_JM" && s(i).names == "notJM1"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_2_JM" && s(i).names == "notJM2"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_1_REST" && s(i).names == "notREST1"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_2_REST" && s(i).names == "notREST2"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_TM" && s(i).names == "notTM"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_JM" && s(i).names == "notJM"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
                if s(j).names == "TpoR_REST" && s(i).names == "notREST"
                    s(j).data2(time,l)=1-(s(nAnal).data(time,l)-s(i).data(time,l))/s(j).data(time,l);
                end
                
            end
            
        end
    end
end

x=linspace(0,simulSize-1,simulSize)';

for j = 1:1:nAnal
    for i=1:1:nAnal
        if s(j).names == "JAK2_1_FERM-SH2" && s(i).names == "JAK2_2_FERM-SH2"
            s(j).data3(:,1)=mean([s(j).data2 s(i).data2]')';
            s(j).data3(:,2)=std([s(j).data2 s(i).data2]')'/sqrt(n*2);
        end
        if s(j).names == "JAK2_1_PK" && s(i).names == "JAK2_2_PK"
            s(j).data3(:,1)=mean([s(j).data2 s(i).data2]')';
            s(j).data3(:,2)=std([s(j).data2 s(i).data2]')'/sqrt(n*2);
        end
        if s(j).names == "JAK2_1_TK" && s(i).names == "JAK2_2_TK"
            s(j).data3(:,1)=mean([s(j).data2 s(i).data2]')';
            s(j).data3(:,2)=std([s(j).data2 s(i).data2]')'/sqrt(n*2);
        end
    end
    if nnz(s(j).data3(:,2)) > 0
        finalProfile=[x s(j).data3(:,1) s(j).data3(:,2)];
        dlmwrite(sprintf('sasa_%s.dat',s(j).names), finalProfile, 'delimiter', '\t')
    else
        continue
    end
end


