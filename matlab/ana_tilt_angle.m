%%
clear all

%Description: Calculating tilting angles.
%Notes: Assumes specific input data structure (GROMACS).

n=10;

fid = fopen('1-ot_0-10000ns.xvg');
for k=1:17
    fgetl(fid);
end
c = textscan(fid,'%f%f','Delimiter','\n');
fclose(fid);

time=c{1,1};

taulukko=zeros(n,max(size(c{1,1})));

for l = 1:1:n
        
        fid = fopen(sprintf('%i-ot_0-10000ns.xvg',l));
        for k=1:17
            fgetl(fid);
        end
        c = textscan(fid,'%f%f','Delimiter','\n');
        
        taulukko(l,:)=c{1,2}';
        
        fclose(fid);

end

profile=mean(taulukko)';
error=std(taulukko)'/sqrt(n);

finalErrors=[time(:),profile(:)+error(:);time(end:-1:1),profile(end:-1:1)-error(end:-1:1)];
finalProfile=[time(:),profile(:)];
meanTilt=mean(finalProfile(:,2));
stdTilt=mean(error);
meanData=[meanTilt stdTilt];

[filepath,name,ext] = fileparts(fileparts(fileparts(fileparts(pwd))));

dlmwrite(sprintf('jak2_%s_data.dat',name), finalProfile, 'delimiter', '\t')
dlmwrite(sprintf('jak2_%s_err.dat',name), finalErrors, 'delimiter', '\t')
dlmwrite(sprintf('jak2_%s_mean.dat',name), meanData, 'delimiter', '\t')


