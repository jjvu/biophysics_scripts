%%
clear all

%Description: Calculating contacts between two molecular entities from MD simulations.
%Notes: Assumes specific input data structure (GROMACS).

n=4;
residueCount=307;
proteinStart=4;

pTable=zeros(residueCount,n);
hTable=zeros(residueCount,n);

for l = 1:1:n
    for i = proteinStart:1:(proteinStart+residueCount-1) 
            if exist(sprintf('%i/POPC_polar-Sfh1_r%d.xvg',l,i))
                fid = fopen(sprintf('%i/POPC_polar-Sfh1_r%d.xvg',l,i));
                %discard first 100 ns (1:39):
                for k=1:24
                    fgetl(fid);
                end
                c = textscan(fid,'%f%f','Delimiter','\n');
                c2 = zeros(max(size(c{1,2})),1);
                for k = 1:1:max(size(c{1,2}))
                    if c{1,2}(k,1) <= 0.6
                        c2(k,1) = 1;
                    else
                        c2(k,1) = 0;
                    end
                    c3=sum(c2);
                end
                pTable(i-(proteinStart-1),l)=c3;          
                fclose(fid);
            else
                pTable(i-(proteinStart-1),l)=0; 
            end
            if exist(sprintf('%i/POPC_hydrophobic-Sfh1_r%d.xvg',l,i))
                fid = fopen(sprintf('%i/POPC_hydrophobic-Sfh1_r%d.xvg',l,i));
                %discard first 100 ns (1:39):
                for k=1:24
                    fgetl(fid);
                end
                c = textscan(fid,'%f%f','Delimiter','\n'); 
                c2 = zeros(max(size(c{1,2})),1);
                for k = 1:1:max(size(c{1,2}))
                    if c{1,2}(k,1) <= 0.6
                        c2(k,1) = 1;
                    else
                        c2(k,1) = 0;
                    end
                    c3=sum(c2);
                end
                hTable(i-(proteinStart-1),l)=c3;          
                fclose(fid);
            else
                hTable(i-(proteinStart-1),l)=0;
            end        
    end
end

simulSize=max(size(c{1,1}));

pCat=pTable/simulSize;
hCat=hTable/simulSize;

pAvg=mean(pCat')';
hAvg=mean(hCat')';
pErr=std(pCat')'/sqrt(n*2);
hErr=std(hCat')'/sqrt(n*2);

x=linspace(4,310,307)';

profile=[x pAvg pErr hAvg hErr];

dlmwrite('residual_lipids.dat', profile, 'delimiter', '\t')

%% For each replica separately

for j=1:1:n
    
    profile1=[x pCat(:,j)];
    profile2=[x hCat(:,j)];
    
    dlmwrite(sprintf('polar_residual_lipids_%d.dat',j), profile1, 'delimiter', '\t')
    dlmwrite(sprintf('hydrophobic_residual_lipids_%d.dat',j), profile2, 'delimiter', '\t')
    
end
