%%
clear all

%Description: Calculating a number of particles inside an ellipsoid (3D) from MD simulations.
%Notes: Assumes specific input data structure (GROMACS). Uses 'inpolyhedron.m' by Sven Holcombe (www.mathworks.com).

% Data
B = importdata('total-ana.txt');
C = importdata('total-ana_selection.txt');

% Matrix to which the permeation values are calculated
dataTable = zeros(21,2);
% Time in microseconds
time=B(:,1)/1000000;
dataTable(:,1) = time;

for i=1:1:21

% Get rid of the time column
data=B(i,2:max(size(B)));
data2=C(i,2:max(size(C)));

% Shape data into x y z form
shapedData = vec2mat(data,3);
shapedData2 = vec2mat(data2,3);

% Define x y z separately - needed for fitting
x=shapedData(:,1);
y=shapedData(:,2);
z=shapedData(:,3);

% Fitting
[ center, radii, evecs, v, chi2 ] = ellipsoid_fit_new( shapedData );

mind = min( [ x y z ] );
maxd = max( [ x y z ] );
nsteps = 50;
step = ( maxd - mind ) / nsteps;
[ x, y, z ] = meshgrid( linspace( mind(1) - step(1), maxd(1) + step(1), nsteps ), linspace( mind(2) - step(2), maxd(2) + step(2), nsteps ), linspace( mind(3) - step(3), maxd(3) + step(3), nsteps ) );
Ellipsoid = v(1) *x.*x +   v(2) * y.*y + v(3) * z.*z + ...
          2*v(4) *x.*y + 2*v(5)*x.*z + 2*v(6) * y.*z + ...
          2*v(7) *x    + 2*v(8)*y    + 2*v(9) * z;

% Which waters are inside the fit? which are out?
fv = isosurface( x, y, z, Ellipsoid, -v(10) );  
fv.faces = fliplr(fv.faces);     
in = inpolyhedron(fv, shapedData2);     

dataTable(i,2) = max(size(in))-nnz(in);

% Show the results
% figure        
% plot3(shapedData2(in,1),shapedData2(in,2),shapedData2(in,3),'bo','MarkerFaceColor','b')
% plot3(shapedData2(~in,1),shapedData2(~in,2),shapedData2(~in,3),'ro'), axis image

end

% Show the results
% figure
% plot(dataTable(:,1),dataTable(:,2));
% % plot(log(dataTable(:,1)),dataTable(:,2));

currentFolder=pwd;
[~,parentDir,~]=fileparts(fileparts(fileparts(currentFolder)));
[~,parentDirRep,~]=fileparts(currentFolder);
    
profileName = sprintf('%s-water_permeation_%s.dat', parentDirRep, parentDir);
dlmwrite(profileName, dataTable, 'delimiter', '\t')

 %% Plot the generated surface

fprintf( 'Ellipsoid center: %.5g %.5g %.5g\n', center );
fprintf( 'Ellipsoid radii: %.5g %.5g %.5g\n', radii );
fprintf( 'Ellipsoid evecs:\n' );
fprintf( '%.5g %.5g %.5g\n%.5g %.5g %.5g\n%.5g %.5g %.5g\n', ...
    evecs(1), evecs(2), evecs(3), evecs(4), evecs(5), evecs(6), evecs(7), evecs(8), evecs(9) );
fprintf( 'Algebraic form:\n' );
fprintf( '%.5g ', v );
fprintf( '\nAverage deviation of the fit: %.5f\n', sqrt( chi2 / size( x, 1 ) ) );
fprintf( '\n' );
 
% Draw data
figure,
plot3( x, y, z, '.r' );
hold on;
 
% Draw fit
 mind = min( [ x y z ] );
 maxd = max( [ x y z ] );
 nsteps = 50;
 step = ( maxd - mind ) / nsteps;
 [ x, y, z ] = meshgrid( linspace( mind(1) - step(1), maxd(1) + step(1), nsteps ), linspace( mind(2) - step(2), maxd(2) + step(2), nsteps ), linspace( mind(3) - step(3), maxd(3) + step(3), nsteps ) );
 
 Ellipsoid = v(1) *x.*x +   v(2) * y.*y + v(3) * z.*z + ...
           2*v(4) *x.*y + 2*v(5)*x.*z + 2*v(6) * y.*z + ...
           2*v(7) *x    + 2*v(8)*y    + 2*v(9) * z;
 p = patch( isosurface( x, y, z, Ellipsoid, -v(10) ) );
 hold off;
 set( p, 'FaceColor', 'g', 'EdgeColor', 'none' );
 view( -70, 40 );
 axis vis3d equal;
 camlight;
 lighting phong;

% Plot the included water molecules
 fv = isosurface( x, y, z, Ellipsoid, -v(10) );
 fv.faces = fliplr(fv.faces);
 in = inpolyhedron(fv, shapedData2);
 
 figure, hold on, view(3)
 plot3(shapedData2(in,1),shapedData2(in,2),shapedData2(in,3),'bo','MarkerFaceColor','b')
 plot3(shapedData2(~in,1),shapedData2(~in,2),shapedData2(~in,3),'ro'), axis image
