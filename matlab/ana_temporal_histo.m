%%
clear all

%Description: Calculating a three dimensional histogram (temporal dimension to be indicated by color)
%Notes: Assumes specific input data structure (GROMACS).

n=8;
simulSize=101;
dataTable=zeros(simulSize,n);

counter=1;
for l = 1:1:2
    
    fid = fopen(sprintf('%d-mindist-EpoR_ANA1-JAK2_ANA1.xvg',l));
    for k=1:24
        fgetl(fid);
    end
    c = textscan(fid,'%f%f','Delimiter','\n');
    dataTable(:,counter) = c{1,2};
    fclose(fid);
    
    counter=counter+1;
    
    fid = fopen(sprintf('%d-mindist-EpoR_ANA2-JAK2_ANA2.xvg',l));
    for k=1:24
        fgetl(fid);
    end
    c = textscan(fid,'%f%f','Delimiter','\n');
    dataTable(:,counter) = c{1,2};
    fclose(fid);
    
    counter=counter+1;

end

for l = 3:1:4
    
    fid = fopen(sprintf('%d-mindist-EpoR_ANA1-JAK2_ANA2.xvg',l));
    for k=1:24
        fgetl(fid);
    end
    c = textscan(fid,'%f%f','Delimiter','\n');   
    siz=max(size(c{1,2}));
    c2=[c{1,2}; zeros(1,101-siz)'];    
    dataTable(:,counter) = c2;
    fclose(fid);
    
    counter=counter+1;
    
    fid = fopen(sprintf('%d-mindist-EpoR_ANA2-JAK2_ANA1.xvg',l));
    for k=1:24
        fgetl(fid);
    end
    c = textscan(fid,'%f%f','Delimiter','\n');   
    siz=max(size(c{1,2}));
    c2=[c{1,2}; zeros(1,101-siz)'];
    dataTable(:,counter) = c2;
    fclose(fid);
    
    counter=counter+1;

end

x=linspace(0,1000,101)';
finalTable=[x dataTable];

tempVector=dataTable(:);
tempX=[x; x; x; x; x; x; x; x];
finalVector=[tempX tempVector];

indices = find(finalVector(:,2)==0);
finalVector(indices,:) = [];

[~,idx] = sort(finalVector(:,1));
finalVector2 = finalVector(idx,:);


dlmwrite('profiles_switch.dat', finalTable, 'delimiter', '\t')

window_size=10;
interfaceInterval = 40;

% Generating matrix
areaProfile=finalVector2;

theArray = cell(1,interfaceInterval);
countArray = ones(1,40);
for j = 1:1:max(size(areaProfile))
    index1 = int32(areaProfile(j,2)*10);
    theArray{countArray(1,index1),index1} = areaProfile(j,1);
    countArray(1,index1) = countArray(1,index1) + 1;
end

% Zero-padding
theArray{100,interfaceInterval} = [];
idxZeros = cellfun(@(c)(isequal(c,0)), theArray);
theArray(idxZeros) = {10};
tf = cellfun('isempty',theArray);
theArray(tf) = {0};
      
% To generate Tikz compatible output
[rows,cols] = size(theArray);
tikzArray = zeros(rows*cols,3);
counter=1;
fileID = fopen('switch_tikz.txt','w');
for colsArray = 1:1:cols
    for rowsArray = 1:1:rows
        tikzArray(counter,:)=[colsArray rowsArray cell2mat(theArray(rowsArray,colsArray))];
        fprintf(fileID,'%d %d %d\\\\\n',tikzArray(counter,1),tikzArray(counter,2),tikzArray(counter,3));
        counter=counter+1;
    end
end
fclose(fileID);
