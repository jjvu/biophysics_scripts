import sys,os
import numpy as np
import operator
from collections import Counter
from matplotlib import cm
import matplotlib as mpl
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from IPython.display import set_matplotlib_formats

#Description: Calculating and plotting ramachandran plot for protein residue interaction distances.
#usage: "python make_ramachandran_plot.py"
#Notes: filenames need to be manually set. 

# Setting plot figure size, font etc.
%matplotlib inline

set_matplotlib_formats('pdf', 'png')
plt.rcParams['savefig.dpi'] = 600
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 20, 20
plt.rcParams['axes.labelsize'] = 50
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 30
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "sans-serif"
plt.rcParams['font.serif'] = "cm"
plt.rcParams['text.latex.preamble'] = r"\usepackage{subdepth}, \usepackage{type1cm}"

# Axis Labels
DIST_1_LABELS=[
"r194-r137",
"r230-r194",
]

nbins=100
binsize=2.5/float(nbins)

rama = []
for i in range(1,5):
    rama.append(np.zeros((nbins, nbins)))
    
nres=0       
for j in range(1,5):
    MAP1 = np.loadtxt(str(j)+"-"+"r230-r194.xvg", comments=["@","#"])
    MAP2 = np.loadtxt(str(j)+"-"+"r194-r137.xvg", comments=["@","#"])
    for ts in range(0,len(MAP1)):
        dihedr1 = MAP1[ts,1::]
        dihedr2 = MAP2[ts,1::]
        index1=int((dihedr1)/binsize) 
        index2=int((dihedr2)/binsize)
        rama[nres][index1][index2] += 1
        
    fig, ax = plt.subplots()

    plt.imshow(rama[nres], origin="lower")

    font = {'family': 'serif',
            'color':  'black',
            'weight': 'bold',
            'size': 30,
            }  
    
    LABELS=[0,0,0.5,1.0,1.5,2.0,2.5]
    
    ax.set_xticklabels(LABELS,rotation=90, fontdict=font, ha="right")
    ax.set_yticklabels(LABELS, fontdict=font, va="top")
    
    ax.set_xlabel("Resid 230 vs resid 194")
    ax.set_ylabel("Resid 194 vs resid 137")

    plt.tick_params(axis='both', which='both', bottom=False, left=False, labelbottom=True, labelleft=True)

    plt.clim(0,100)
    plt.colorbar(fraction=0.027, pad=0.04, aspect=30).set_label("Contact Probability", rotation=270, labelpad=80)
    plt.grid(True, linewidth=1.1)
    plt.savefig("Map"+str(j)+".pdf", dpi=300)
    plt.show()
    
    
    nres += 1
