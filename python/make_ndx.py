import numpy as np

#Description: Generates an index preparation file with each protein residue as an index group. The preparation file can be given as an input to GROMACS index generation.
#Usage: "python make_ndx.py"
#Notes: Index names and residue ranges need to be manually set.

protein1_residues=np.linspace(536,808,273)
protein2_residues=np.linspace(537,808,272)
text_file = open("residues_split.txt", "w")
text_file.write("del2-60\n")
text_file.write("splitch 1\n")
text_file.write("name 2 JH2_1\n")
text_file.write("name 3 JH2_2\n")
counter=4
for i in protein1_residues:
    text_file.write("2&r%i\n" % (i))
    text_file.write("name %s JH2_1-r%i\n" % (counter, i))
    counter=counter+1
for j in protein2_residues:
    text_file.write("3&r%i\n" % (j))
    text_file.write("name %s JH2_2-r%i\n" % (counter, j))
    counter=counter+1
text_file.write("q\n")     
text_file.close()
