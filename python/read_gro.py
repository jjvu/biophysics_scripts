#Description: This sript read a GRO file and removes waters. It also modifies the simulation box size.
#Usage: "python read_gro.py
#Notes: Filenames need to be manually adjusted.


# Initiatiate a list called "lines" and a string called "SOL"
lines=[]
substring='SOL'

with open('test.gro', 'r') as F: 
    # Getting rid of the first and second line before the loop 
    first = F.readline()
    second = F.readline()
    for line in F:
        # If a line does not contain a key word and it has a different number of elements 
        # than three it is regarded as a line with atom info.
        if line.find(substring) == -1 and len(line.split()) != 3:           
            lines.append([int(line[0:5]), line[5:8], line[8:15], int(line[15:20]),
                   float(line[20:28])+2, float(line[28:36])-1, float(line[36:44])+1]) 
        # If a line has three elements it is the last line containing the box size.            
        if len(line.split()) == 3: 
            last=line.split()
            box = [2.0*float(last[0]), 2.0*float(last[1]), 2.0*float(last[2])]
        # Because the first and the second line have already been read, all the other 
        # lines are the ones with "SOL", and they are discarded (i.e. not stored to "lines")  
        else: 
            continue
            
# The number of atoms in the new system is the same as the atom number of the last atom in the list.
new_atomno = lines[-1][3]

with open("fixed.gro", "w") as text_file:
    text_file.write("New gro file\n")
    text_file.write(str(new_atomno) +"\n")
    s="{0[0]:>5}{0[1]:<3}{0[2]:>7}{0[3]:>5}{0[4]:>8.3f}{0[5]:>8.3f}{0[6]:>8.3f}\n"
    for atom in lines: 
        text_file.write(s.format(atom))
    text_file.write("{0[0]:>10.5f}{0[1]:>10.5f}{0[2]:>10.5f}\n".format(box)) 
