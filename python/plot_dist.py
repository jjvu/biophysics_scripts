import numpy as np
import matplotlib.pyplot as plt

#Description: Plotting multiple graphs of temporal data (multiple systems + repetitions)
#usage: "python plot_dist.py"
#Notes: Target data files and their labels (system name) need to be manually set. Assumes columnar data where the first column is time and subsequent column are repetitions

counter=0
fig, ax = plt.subplots()
colorlist=[(1,0,1),(1,0.5,0.5),(0.5,0.25,1),(0,0,1),(0,1,0),(1,1,0),(0,1,1),]

for fname, label in [('V617F/2-production/ana_joni/0-ana-dist/data_fix.dat', 'VF'), ('wt/2-production/ana_joni/0-ana-dist/data_fix.dat',' wt'), ('E592R/2-production/ana_joni/0-ana-dist/data_fix.dat', 'ER'), ('F595A/2-production/ana_joni/0-ana-dist/data_fix.dat', 'FA'), ('K539L/2-production/ana_joni/0-ana-dist/data_fix.dat', 'KL'), ('V617F_E592R/2-production/ana_joni/0-ana-dist/data_fix.dat', 'VF/ER'), ('V617F_F595A/2-production/ana_joni/0-ana-dist/data_fix.dat', 'VF/FA')]:
    print(counter)
    data=np.loadtxt(fname)
    row, col = data.shape
    X=data[:,0]
    Y=data[:,1:col-1]
    ax.plot(X,Y,color=colorlist[counter],linestyle='-',linewidth=0.75,label=label)
    counter=counter+1
leg = ax.legend();    
plt.ylim((0.5,2.5))
plt.show()
#plt.save('figure.png')

