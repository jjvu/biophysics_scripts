import numpy as np
from matplotlib import cm
import matplotlib as mpl
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from IPython.display import set_matplotlib_formats
import re
import os
import itertools
from pathlib import Path

#Description: Calculating and plotting protein RMSD values from MD simulations
#usage: "python plot_rmsd.py"
#Notes: Assumes a specific input folder structure and naming. Assumes also specific input data structure.

# Setting plot figure size, font etc.

set_matplotlib_formats('pdf', 'png')
plt.rcParams['savefig.dpi'] = 600
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 20, 20
plt.rcParams['axes.labelsize'] = 50
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 30
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "sans-serif"
plt.rcParams['font.serif'] = "cm"
plt.rcParams['text.latex.preamble'] = r"\usepackage{subdepth}, \usepackage{type1cm}"

# Analysis and plotting
systemName = os.getcwd().split('/')[-3]
systemName2 = os.getcwd().split('/')[-2]
DATA = []
for j in range(1,3):
    DATA.append(list(np.loadtxt("0-ana-rmsd/"+str(j)+"-rmsd_fit_activator.xvg", comments=["@","#"], usecols=(1))))

print("number of replicas ", len(DATA))

lengths = []
nLines = len(DATA) #Replicas
for i in range(len(DATA)):
    lengths.append(len(DATA[i]))
        
avg = np.zeros(min(lengths))
err = np.zeros(min(lengths))
mini = min(lengths)
labels=range(0,mini*10,10)

for j in range(mini):
    temp = []
    for i in range(len(DATA)):
        temp.append(DATA[i][j])
        avg[j] = np.mean(temp)
        err[j] = np.std(temp)/np.sqrt(nLines)

with open(("0-ana-rmsd/data_%s_%s.dat" % (systemName, systemName2)), "w") as text_file:
    s="{0[0]:>8.3f}{0[1]:>8.3f}\n"
    text_file.write("\pgfplotstableread{ % data\n")
    text_file.write("Label \t Value \t Error\n")
    text_file.writelines(map("{:8.3f}\t{:8.3f}\t{:8.3f}\n".format, labels, avg, err))
    text_file.write("}\data\n")
    
plt.errorbar(labels,avg,yerr=err)   
plt.axis([0, 120, 0, 1.5])
#plt.fill_between(labels, avg-err, avg+err)
plt.show()   
