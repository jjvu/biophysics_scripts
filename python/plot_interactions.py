import numpy as np
from matplotlib import cm
import matplotlib as mpl
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from IPython.display import set_matplotlib_formats
import re
import os
import itertools
from pathlib import Path

#Description: Calculating and plotting protein-protein contacts
#usage: "python plot_interactions.py"
#Notes: residue ranges and input paths need to be manually set. 

# Setting plot figure size, font etc.

set_matplotlib_formats('pdf', 'png')
plt.rcParams['savefig.dpi'] = 600
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 20, 20
plt.rcParams['axes.labelsize'] = 50
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 30
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "sans-serif"
plt.rcParams['font.serif'] = "cm"
plt.rcParams['text.latex.preamble'] = r"\usepackage{subdepth}, \usepackage{type1cm}"

# Define residue ranges
aResidues=list(range(536,820))
rResidues=list(range(830,1133))

aStart=536
rStart=830

aBins=284
rBins=303

aList=list(range(aBins))
rList=list(range(rBins))

rama=[]

# Calculate Data
for j in range(1,3):
    rama.append(np.zeros((aBins,rBins)))
    for i1 in aResidues:
        for i2 in rResidues:
            
            my_file = Path("4-production/ana/mindist_residues_split/"+str(j)+"/activator-r"+str(i1)+"_receiver-r"+str(i2)+".xvg")
            if my_file.exists():
                MAP = np.loadtxt(my_file, comments=["@","#"])
                MAP1 = np.loadtxt(my_file, comments=["@","#"])
            MAP[MAP>0.6]=0
            MAP[MAP1<=0.6]=1
            rama[j-1][i1-aStart][i2-rStart] = np.sum(MAP[:,1::], axis=0)/MAP.shape[0]
    
    # Plot data
    fig, ax = plt.subplots()

    plt.xlim((820, 1010))
    plt.ylim((750, 830))
    plt.contour(rResidues, aResidues, rama[j-1],cmap=plt.cm.jet) 
    font = {'family': 'serif',
            'color':  'black',
            'weight': 'bold',
            'size': 30,
            }
    plt.tick_params(axis='both', which='both', bottom=False, left=False, labelbottom=True, labelleft=True)
    plt.colorbar(fraction=0.027, pad=0.04, aspect=30).set_label("Contact Probability", rotation=270, labelpad=80)
    plt.show()

# Calculate average and plot
meanMatrix=np.zeros((aBins,rBins))
errorMatrix=np.zeros((aBins,rBins))

for index1 in aList:
    for index2 in rList:
        temp = []
        for replica in range(0,2):
            temp.append(rama[replica][index1][index2])
            meanMatrix[index1][index2] = np.mean(temp)
            errorMatrix[index1][index2] = np.std(temp)/np.sqrt(len(temp))
            
fig, ax = plt.subplots()

plt.xlim((820, 1010))
plt.ylim((750, 830))
plt.contour(rResidues, aResidues, meanMatrix,cmap=plt.cm.jet) 
plt.tick_params(axis='both', which='both', bottom=False, left=False, labelbottom=True, labelleft=True)
plt.colorbar(fraction=0.027, pad=0.04, aspect=30).set_label("Contact Probability", rotation=270, labelpad=80)
plt.show() 

# Output tabulated interactions
JH2_residue_names="VFHKIRNEDLIFNESLGQGTFTKIFKGVRREVGDYGQLHETEVLLKVLDKAHRNYSESFFEAASMMSKLSHKHLVLNYGVCVCGDENILVQEFVKFGSLDTYLKKNKNCINILWKLEVAKQLAWAMHFLEENTLIHGNVCAKNILLIREEDRKTGNPPFIKLSDPGISITVLPKDILQERIPWVPPECIENPKNLNLATDKWSFGTTLWEICSGGDKPLSALDSQRKLQFYEDRHQLPAPKWAELANLINNCMDYEPDFRPSFRAIIRDLNSLFTPDYELLTEN"
JH1_residue_names="LGFSGAFEDRDPTQFEERHLKFLQQLGKGNFGSVEMCRYDPLQDNTGEVVAVKKLQHSTEEHLRDFEREIEILKSLQHDNIVKYKGVCYSAGRRNLKLIMEYLPYGSLRDYLQKHKERIDHIKLLQYTSQICKGMEYLGTKRYIHRDLATRNILVENENRVKIGDFGLTKVLPQDKEYYKVKEPGESPIFWYAPESLTESKFSVASDVWSFGVVLYELFTYIEKSKSPPAEFMRMIGNDKQGQMIVFHLIELLKNNGRLPRPDGCPDEIYMIMTECWNNNVNQRPSFRDLALRVDQIRDNMAG"

jh2=list(JH2_residue_names)
jh1=list(JH1_residue_names)

nameMatrix = np.empty([aBins, rBins], dtype="<U10")

for index1 in aList:
    for index2 in rList:
        nameMatrix[index1][index2] = '{}{}-{}{}'.format(jh2[index1],aResidues[index1],jh1[index2],rResidues[index2])

vectorized_array = np.asarray(meanMatrix).reshape(-1)
sorted_array = np.sort(vectorized_array)
sortIndices = np.argsort(vectorized_array)
reverse_array = sorted_array[::-1]

vectorized_namearray = np.asarray(nameMatrix).reshape(-1)
sorted_namearray = vectorized_namearray[sortIndices]
reverse_namearray = sorted_namearray[::-1]

with open("residue-by-residue_interactions.dat", "w") as text_file:
    text_file.writelines(map("{}\t{:8.3f}\n".format, reverse_namearray, reverse_array))

