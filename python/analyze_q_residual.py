#!/usr/bin/python

#Description: This sript calculates the number of native contacts (Q) for a single protein residue.
#Usage: "python analyze_q_residual.py residue_number mol.gro traj.xtc
#Notes: Takes into account all the side-chain vs side-chain contacts (even the trivial contacts between neighbouring residues)

import sys
import numpy as np
import matplotlib.pyplot as plt
import MDAnalysis
from MDAnalysis.analysis import contacts

res=sys.argv[1]
groFile=sys.argv[2]
trajFile=sys.argv[3]

u = MDAnalysis.Universe(groFile, trajFile)

#Selections for contact analysis:
#each residue at a time
#backbone of surrounding residues not accounted
sel="resid "+str(res)+" and not (name C or name O or name N)"
sel2="(not resid "+str(res)+" and not resid "+str(int(res)-1)+" and not resid "+str(int(res)+1)+") and not (name C or name O or name N)"
selli = u.select_atoms(sel)
selli2 = u.select_atoms(sel2)
ca1 = contacts.Contacts(u, selection=(sel, sel2), refgroup=(selli, selli2), radius=6.0,  method="soft_cut")

# iterate through trajectory and perform analysis of "native contacts" Q
ca1.run()

# print number of averave contacts
average_contacts = np.mean(ca1.timeseries[:, 1])
print('average contacts = {}'.format(average_contacts))

# plot time series q(t)
fig, ax = plt.subplots()
ax.plot(ca1.timeseries[:, 0], ca1.timeseries[:, 1])
ax.set(xlabel='frame', ylabel='fraction of native contacts', title='Native Contacts, average = {:.2f}'.format(average_contacts))
plt.savefig(f'fig_{res}.png')

lenData=len(ca1.timeseries[:,1])
lenData_fix=int(np.ceil(lenData/4))
average_contacts1 = np.mean(ca1.timeseries[-1:-lenData_fix:-1, 1])
average_contacts1_fix=np.array([average_contacts1])

np.savetxt(f'saved_{res}.out', average_contacts1_fix, fmt = '%.6f')
