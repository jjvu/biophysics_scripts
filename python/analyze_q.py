import sys
import numpy as np
import matplotlib.pyplot as plt
import MDAnalysis
from MDAnalysis.analysis import contacts

#Description: This sript calculates the fraction of total number of native contacts (Q).
#Usage: "python analyze_q.py mol.gro traj.xtc
#Notes: Takes into account all the side-chain vs side-chain contacts (even the trivial contacts between neighbouring residues)

groFile=sys.argv[1]
trajFile=sys.argv[2]

u = MDAnalysis.Universe(groFile, trajFile)

# Selections for contact analysis:
# Backbone residues are not accounted
sel="not (name C or name O or name N)"
sel2="not (name C or name O or name N)"
selli = u.select_atoms(sel)
selli2 = u.select_atoms(sel2)
ca1 = contacts.Contacts(u, selection=(sel, sel2), refgroup=(selli, selli2), radius=6.0,  method="soft_cut")
# Iterate through trajectory and perform analysis of Q
ca1.run()
# Print number of average contacts
average_contacts = np.mean(ca1.timeseries[:, 1])
print('average contacts = {}'.format(average_contacts))
# Plot time series q(t)
fig, ax = plt.subplots()
ax.plot(ca1.timeseries[:, 0], ca1.timeseries[:, 1])
ax.set(xlabel='frame', ylabel='fraction of native contacts', title='Native Contacts, average = {:.2f}'.format(average_contacts))
#fig.show()
plt.savefig(f'fig_q.png')

profile1=ca1.timeseries[:, 1]

np.savetxt(f'saved_q.out', profile1, fmt = '%.6f')
