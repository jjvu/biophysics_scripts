import MDAnalysis
import os,sys
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from pylab import *

#Description: Calculating cholesterol flip-flops between lipid bilayer leaflets
#usage: "python analyze_cholesterol_flip_flop.py mol.gro traj.xtc out.dat"
#Notes: residue ranges and input paths need to be manually set. 

groFile=sys.argv[1]
trajFile=sys.argv[2]
outFile=sys.argv[3]

u = MDAnalysis.Universe(groFile, trajFile)

selec1=u.select_atoms("resname POPC and name P")
selec2=u.select_atoms("resname CHL1 and name O3")

time=[]
cholesterols=[]
for ts in u.trajectory:
	time.append([])
	time[-1].append(ts.time)
	cholesterols.append([])
	com=np.mean(selec1.positions[:,[2]])
	cholCounter=0
	for chol in selec2.positions[:,[2]]:
		if chol>=com:
			cholCounter=cholCounter+1
		else:
			cholCounter=cholCounter-1
	cholesterols[-1].append(cholCounter)

np.savetxt(outFile, cholesterols, fmt = '%.6f')
