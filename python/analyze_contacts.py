#!/usr/bin/python

#Description: This sript performs a simple contacts analysis based on GROMACS output.
#Usage: "python analyze_contacts.py FILE.xvg
#Notes: Number of rows to skip needs to be manually set (assumes older GROMACS version 4.x).

import MDAnalysis
import os,sys
import numpy as np 
import matplotlib as mpl 
mpl.use('agg')
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import mmap

path=os.getcwd()

if len(sys.argv) == 1:
    print "You can also give filename as a command line argument"
    outfilename = raw_input("Input name for output file: ")
else:
    outfilename = "fig1.png"
    
resList = []    
list_dir = []
list_dir = os.listdir(path)
for file in list_dir:
    if file.endswith('.xvg'): 
        resList.append(int(file.split('.')[0]))

resList.sort()        
firstResidue = min(resList)   
lastResidue = max(resList)      
avgs = []
nResidues = range(firstResidue,lastResidue+1)

hbond = 0
        
for residue in nResidues:
    datafile = "%s.xvg" % (residue)
    f = open(datafile)
    s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
    if s.find('Hydrogen bonds') != -1:
        time,x,y = np.genfromtxt(datafile, unpack=True, skiprows=20)
        hbond = 1
    elif s.find('Number of Contacts') != -1:
        time,x = np.genfromtxt(datafile, unpack=True, skiprows=19)
    avgs.append(np.mean(x))
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)    
ax.plot(resList,avgs)
if hbond == 1:
    ax.axis([20, lastResidue, 0, 3])
else:
    ax.axis([20, lastResidue, 0, 600])
fig.savefig(outfilename)
   

            
        
        
    
