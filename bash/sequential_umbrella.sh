#!/bin/bash
#SBATCH --job-name=NAME
#SBATCH --account=ACCOUNT
#SBATCH --partition=medium
#SBATCH --time=36:00:00
#SBATCH --nodes=8
#SBATCH --cpus-per-task=4
#SBATCH --ntasks-per-node=32

#Description: This script run a whole umbrella sampling protocol in a sequential fashion. 
#Notes: Names need to be manually set. This script uses GROMACS version 2020.

# set the number of threads based on --cpus-per-task
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# Bind OpenMP threads to cores
export OMP_PLACES=cores

module load gcc/9.3.0  
module load openmpi/4.0.3
module load gromacs/2020.2

GRO_INI="0.gro"
NAME="pull"
ZI=23.70 #Initial distance between the reference and pulling group (in angstroms)
DZ=1.2  #Difference distance between umbrella windows (in angstroms)
NW=24 #Number of windows. The sampled distance is BZ x NW 

# Read window number being sampled
if [ ! -f tmp_window ]; then
  echo "0" > tmp_window
fi

W=$(cat tmp_window)
if (( W > NW )); then
  exit 0
fi

# Calculate distance between reference and pulling group being sampled
D=$(echo "scale=1;$ZI+$DZ*$W" | bc) #In Angstoms
DNM=$(printf "%0.3f\n" $(bc -q <<< scale=3\;$D/10))
DPREV=$(echo "scale=1;$ZI+$DZ*($W-1)" | bc)  #In Angstoms

echo "Window number = $W of $NW"
echo "Current window distance = $D Ang($DNM nm)"
echo "Previous window distance = $DPREV Ang" 

mkdir ${NAME}_d${D}
cd ${NAME}_d${D}

NAMEW=${NAME}_d${D}

# Check if current window is a new window or a continuation
if [ ! -f ${NAMEW}.tpr ]; then
  if (( W == 0 )); then
    GRO_PREV=../${GRO_INI}
  else 
    GRO_PREV=../${NAME}_d${DPREV}/${NAME}_d${DPREV}.gro
  fi
  cp -r ../toppar .
  cp -r ../restraints .
  cp ../*.top ${NAME}.top
  cp ../*.ndx ${NAME}.ndx
  sed -e "s:XXX:$DNM:" ../*_umbrella_dXX.mdp > ${NAMEW}.mdp
  cp $GRO_PREV  $NAME_d${DPREV}.gro
  pwd
  gmx_mpi grompp -f ${NAMEW}.mdp -c $GRO_PREV -p ${NAME}.top -r ../0.gro -n ${NAME}.ndx -o ${NAMEW}.tpr -maxwarn 2
fi

# Launch the simulation
srun gmx_mpi mdrun -v -deffnm ${NAMEW} -cpi ${NAMEW}.cpt -pf ${NAMEW}-pf.xvg -px ${NAMEW}-px.xvg -stepout 10000 -dlb auto -maxh 35.9

# Check if sampling is completed in the current simulation window
if [ -f ${NAMEW}.gro ]; then
  echo $((W+1)) > ../tmp_window
fi

# Relaunch the script
cd ..
sleep 240
sbatch umbrella_mahti.sh
