#!/bin/bash

#Description: This analyses residue-by-residue contacts of two (dimerized) proteins from MD simulations.
#Notes: Filenames need to be manually set. Index preparation file "residues_split.txt" needs to be generated with other scripts. Assumes GROMACS version 2016 or later.

function fcomp() {
    awk -v n1=$1 -v n2=$2 'BEGIN{ if (n1<=n2) exit 0; exit 1}'
}
   
begin=$(date +"%s")
 
mkdir 2-mindist_residues_split
mkdir 2-numcont_residues_split

gmx make_ndx -f ../1/total-ana.gro -o ana_res_split.ndx < residues_split.txt

for j in $(seq 482 520)
do	
    for i in $(seq 482 520)                                                            
    do
        
	echo "TpoR1-r${j}" "TpoR2-r${i}" | gmx mindist -f 0-ana-numcont2/contacts/total.xtc -s 0-ana-numcont2/protein.tpr -n ana_res_split.ndx -od 2-mindist_residues_split/$SYSTEM/TpoR1-r${j}_TpoR2-r${i}.xvg -on 2-numcont_residues_split/$SYSTEM/TpoR1-r${j}_TpoR2-r${i}.xvg

	avgnc=($(gmx analyze -f 2-numcont_residues_split/$SYSTEM/TpoR1-r${j}_TpoR2-r${i}.xvg | grep -i 'SS1'))
	if fcomp ${avgnc[1]} 0; then
	    rm 2-numcont_residues_split/$SYSTEM/TpoR1-r${j}_TpoR2-r${i}.xvg
	fi
	
    done	
done

termin=$(date +"%s")
difftimelps=$(($termin-$begin))
echo "$(($difftimelps / 60)) minutes and $(($difftimelps % 60)) seconds elapsed for Script Execution."



