#!/bin/bash

#Description: This script does an RMSD-based clustering to a protein molecule. The aim is to find the most populated conformations in an MD simulation.
#Notes: Names and RMSD cutoff values need to be manually set. Assumes GROMACS version 2016 or later.

mkdir clusters

awk 'BEGIN {print "del2-60"}' >prep_rmsd.txt
awk 'BEGIN {print "splitch 1"}' >>prep_rmsd.txt
awk 'BEGIN {print "name 2 TpoR_1"}' >>prep_rmsd.txt
awk 'BEGIN {print "name 3 TpoR_2"}' >>prep_rmsd.txt
awk 'BEGIN {print "q"}' >>prep_rmsd.txt

gmx make_ndx -f ../../1-prep/step6.6_equilibration.gro -o ndx.ndx <prep_rmsd.txt

# PREPARE FILES (Trajectory is based on previous contact analysis to recognize dimerized frames)
echo -e "c\nc\nc\nc\nc\nc\nc\nc\n" | gmx trjcat -f 0-ana-numcont2/contacts/1/traj.xtc 0-ana-numcont2/contacts/2/traj.xtc 0-ana-numcont2/contacts/3/traj.xtc 0-ana-numcont2/contacts/4/traj.xtc 0-ana-numcont2/contacts/5/traj.xtc 0-ana-numcont2/contacts/6/traj.xtc 0-ana-numcont2/contacts/7/traj.xtc 0-ana-numcont2/contacts/8/traj.xtc -cat -o clusters/total.xtc -settime
echo "Protein" | gmx convert-tpr -s ../1/md-production.tpr -o clusters/protein.tpr -nsteps -1
echo "TpoR_1" "Protein" | gmx trjconv -f clusters/total.xtc -s ../1/md-production.tpr -n ndx.ndx -o clusters/total_fit.xtc -fit rot+trans
echo "TpoR_1" "Protein" | gmx trjconv -f clusters/total.xtc -s ../1/md-production.tpr -n ndx.ndx -o clusters/total_fit.gro -fit rot+trans -dump 0

# LOOP OVER DESIRED RANGE OF VARIABLES
methods=(linkage jarvis-patrick monte-carlo diagonalization gromos)
cutoffs=(2.2 2.25 2.3 2.35 2.4 2.45 2.5 2.55 2.6 2.65 2.7 2.75 2.8)
i=0
for m in ${methods[*]}; do
  for c in ${cutoffs[*]}; do
    p0[i]=$m
    p1[i]=$c
    ((i++))
done
done

# CUSTOM GROMACS COMMAND
gmxcluster () {

method=${p0[$1]}
cutoff=${p1[$1]}
out="clusters/$method/$cutoff"
mkdir -p $out 

echo "Protein" "Protein" | gmx cluster -f clusters/total.xtc -s clusters/protein.tpr -g $out/cluster.log -dist $out/rmsd-dist.xvg -ev $out/rmsd-eig.xvg -conv $out/mc-conv.xvg -sz $out/clust-size.xvg -tr $out/clust-trans.xpm -ntr $out/clust-trans.xvg -clid $out/clust-id.xvg -cl $out/clusters.pdb -nlevels 100 -cutoff $cutoff -nofit -method $method

}

# RUN COMMAND
for i in {0..65}
do
gmxcluster $i 
done


