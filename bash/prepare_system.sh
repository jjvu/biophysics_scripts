#!/bin/bash

#Description: This script goes through the phases of simulation system preparation.
#Notes: Initial filenames need to be manually set. Phases 1. and 2. are also manual. This script uses GROMACS.

# 1. Modify the structure file and topolygy
# 2. Check system size & center the system

NAME="md-prepare"

# 3. Add waters

# 3.a. Solvate the system
cp CD44.top 3-${NAME}_water.top
genbox -cp  fixed.gro -cs -o 3-${NAME}_water.gro -p 3-${NAME}_water.top

make_ndx -f 3-${NAME}_water.gro -o < indexing1.txt

# 3.b. Minimize the system
grompp -f em.mdp -c 3-${NAME}_water.gro -p 3-${NAME}_water.top -n index.ndx -o 3-${NAME}_water_min.tpr -maxwarn 1 > >(tee stdout.log) 2> >(tee stderr.log >&2)
mdrun -v -deffnm 3-${NAME}_water_min

# 4. Neutralize the system

# 4.a. Calculate number of waters
read name n_waters <<< $(tail -1 3-${NAME}_water.top)
echo "Number of waters = ${n_waters}"

# 4.b. Calculate number of ions used to neutralize the system and the number of water molecules after the addition of salt
read a b c d e charge <<<$(grep "non-zero" stderr.log)
if [ -z $charge  ]; then
    echo "No charge detected"
    charge=0
fi

echo "charge system= $charge"

charge=${charge/\.*}

perl -e '$a=0.15*'$n_waters'/55.5; $ceil = int($a + 0.99); print "$ceil\n"' >& temp_cl     
read number_of_chlorides <<< $(tail -1 temp_cl)                                    
echo "Number of negative ions:  $number_of_chlorides"                                               
number_of_na=$((-1*$charge + $number_of_chlorides)) 
echo "Number of positive ions: $number_of_na"  

# 4.c. Neutralize the system by adding salt
cp  3-${NAME}_water.top  4-${NAME}_salt.top
echo "SOL" |genion -s 3-${NAME}_water_min.tpr -o 4-${NAME}_salt.gro -p 4-${NAME}_salt.top \
		   -pname NA -np $number_of_na -nname CL -nn $number_of_chlorides

sed -i -e 's/NA/NA_d/g' 4-${NAME}_salt.top
sed -i -e 's/CL/CL_d/g' 4-${NAME}_salt.top

make_ndx -f 4-${NAME}_salt.gro -o < indexing1.txt

# 4.d. Minimize the system
grompp -f em1.mdp -c 4-${NAME}_salt.gro -p 4-${NAME}_salt.top -n index.ndx -o 4-${NAME}_salt_min.tpr -maxwarn 1
mdrun -v -deffnm 4-${NAME}_salt_min

# 5. Run equilibration

cp 4-${NAME}_salt.top 5-${NAME}.top

grompp -f md.mdp -c 4-${NAME}_salt_min.gro -p 5-${NAME}.top -n index.ndx -o 5-${NAME}_md.tpr -maxwarn 2
mdrun -v -deffnm 5-${NAME}_md

# Remove unneeded

rm ./#* -f
rm temp_* -f
rm step-* -f
